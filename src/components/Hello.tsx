import * as React from "react";

export interface HelloProps {
    compiler? : string,
    framework? : string
}

export class Hello extends React.Component<HelloProps, any> {
    constructor(props:HelloProps) {
        super(props)
        this.state = {
            compiler: this.props.compiler || "language",
            framework: this.props.framework || "framework"
        }
    }



    render() {
        return(
            <div className="jumbotron-fluid jumbotron"
                style={{
                    background:'orange'
                }}
            >
                <h1>Hello from {this.state.compiler} and {this.state.framework}!</h1>
  
        </div>);
        }
    }
