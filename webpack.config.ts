import * as webpack from "webpack";
import * as path from "path";


const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const isProduction = process
    .argv
    .indexOf('-p') >= 0;
const outPath = path.join(__dirname, './dist');
const srcPath = path.join(__dirname, './src');

const config: webpack.Configuration = {
    entry: {
        main: "./src/index.tsx"
    },
    output: {
        filename: "[name].bundle.js",
        path: outPath,
        publicPath: "./"
    },

    // Enable sourcemaps for debugging webpack's output.
    devtool: "#source-map",
    resolve: {
        // Add '.ts' and '.tsx' as resolvable extensions.
        extensions: [
            ".webpack.js",
            ".web.js",
            ".ts",
            ".tsx",
            ".js",
            "json",
            "css",
            "html"
        ],
        modules: [
            srcPath, path.join(__dirname, './node_modules')
        ],
        mainFields: ['browser', 'main']
    },

    module: {
        rules: [
            { // All files with a '.ts' or '.tsx' extension will be handled by 'ts-loader'.
                test: /\.tsx?$/,
                use: {
                    loader: 'awesome-typescript-loader'
                }
            },
            // static assets
            {
                test: /\.html$/,
                use: {
                    loader: 'html-loader'
                }
            }, {
                test: /\.png$/,
                use: 'url-loader?limit=10000'
            }, {
                test: /\.jpg$/,
                use: 'file-loader'
            }, {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                            query: {
                                modules: true,
                                sourceMap: !isProduction,
                                importLoaders: 1,
                                localIdentName: '[local]__[hash:base64:5]'
                            }
                        }
                    ]
                })
            }, {
                test: /\.(scss)$/,
                use: [{
                    loader: 'style-loader'
                }, {
                    loader: 'css-loader'
                }, {
                    loader: 'postcss-loader',
                    options: {
                        plugins:
                            function () {
                                return [
                                    require('precss'),
                                    require('autoprefixer')
                                ];
                            }
                    }

                }, {
                    loader: 'sass-loader'
                }]
            }]
    },

    devServer: {
        port: 3000,
        host: "localhost",
        hot: true,
        inline: true,
        contentBase: 'src',
        publicPath: '/'
    },
    node: {
        // workaround for webpack-dev-server issue
        // https://github.com/webpack/webpack-dev-server/issues/60#issuecomment-103411179
        fs: 'empty',
        net: 'empty'
    },

    plugins: [new HtmlWebpackPlugin({ template: "src/static/index.html", chunksSortMode: "dependency" })]
};

export default config;