import * as React from "react";
import * as ReactDOM from "react-dom";

require("bootstrap/scss/bootstrap.scss");

const bootstrap = require("bootstrap");

import {Hello} from "./components/Hello";

ReactDOM.render(
    <Hello />, document.getElementById("example")
);
